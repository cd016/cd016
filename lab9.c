#include <stdio.h>

int main()
{
    int num1, num2 ;

    int *ptr1, *ptr2;

    ptr1 = &num1; // ptr1 stores the address of num1
    ptr2 = &num2; // ptr2 stores the address of num2

    printf("Enter any two numbers: ");
    scanf("%d %d", &num1, &num2);

    int temp;
    temp=*ptr1;
    *ptr1=*ptr2;
    *ptr2=temp;
    printf("the swapped numbers are %d & %d",*ptr1,*ptr2);
}

//write your code here