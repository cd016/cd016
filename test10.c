#include <stdio.h>

int main()
{
    int num1, num2, sum,sub=0,mul ;
    float rem,div;
    int *ptr1, *ptr2;

    ptr1 = &num1; // ptr1 stores the address of num1
    ptr2 = &num2; // ptr2 stores the address of num2

    printf("Enter any two numbers: ");
    scanf("%d%d", &num1, &num2);

    sum = *ptr1 + *ptr2;
    printf("Sum = %d", sum);
    if(*ptr1>*ptr2)
    {


        sub = *ptr1 - *ptr2;
        printf("Sub = %d\n", sub);
    }
    else
    {
        int temp;
        temp=*ptr1;
        *ptr1=*ptr2;
        *ptr2=temp;
        sub = *ptr1 - *ptr2;
        printf("\nSubtraction = %d\n", sub);

    }

    mul=*ptr1 * *ptr2;
    printf("multiplication = %d\n", mul);

    div= (*ptr1)/(*ptr2);
    printf("division = %f\n",div);

    rem=*ptr1 % *ptr2;
    printf("remainder = %f\n", rem);
    
    return 0;
}
//write your code here